var gulp = require('gulp')

var webpack = require('gulp-webpack')

var sourcemaps = require('gulp-sourcemaps')
var minify = require('gulp-minify')
var babel = require('gulp-babel')
var jsx = require('gulp-jsx')
// var browserSync = require('browser-sync').create()

gulp.task('jsx', function () {
  return (
    gulp
      .src('./src/react-forms/*.jsx')
      // .pipe(sourcemaps.init())
      // // .pipe(
      // //   jsx({
      // //     factory: 'React.createClass',
      // //   }),
      // // )
      // .pipe(
      //   babel({
      //     presets: ['@babel/env', '@babel/preset-react'],
      //     plugins: ['@babel/plugin-proposal-export-default-from'],
      //   }),
      // )
      .pipe(webpack(require('./webpack.config.js')))
      // .pipe(minify())
      // .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('./src/js/react'))
  )
  // .pipe(browserSync.reload({ stream: true }))
})

gulp.task('default', function () {
  gulp.watch(['./src/react-forms/*.jsx'], gulp.task('jsx'))
})
