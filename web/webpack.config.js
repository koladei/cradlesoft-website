const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

const port = process.env.PORT || 80

module.exports = {
  //   mode: 'development',
  target: 'web',
  entry: path.resolve(__dirname, './src/react-forms/App.jsx'),
  output: {
    path: path.resolve(__dirname, './src/js/react'),
    filename: 'App-min.js',
  },
  devtool: 'inline-source-map',
  plugins: [
    new HtmlWebpackPlugin({ template: './src/index.html' }),
    new MiniCssExtractPlugin(),
  ],
  module: {
    rules: [
      // First Rule
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },

      // Second Rule
      {
        test: /\.css$/,
        use: [
          //   {
          //     loader: 'style-loader',
          //   },
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            // options: {
            //   modules: true,
            //   localsConvention: 'camelCase',
            //   sourceMap: true,
            // },
          },
        ],
      },
    ],
  },
  devServer: {
    host: 'cradlesoft.com',
    port: port,
    watchContentBase: true,
  },
}
