"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

var _reactBootstrap = require("react-bootstrap");

var _officeUiFabricReact = require("office-ui-fabric-react");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// const e = React.createElement
var Modal = /*#__PURE__*/function (_React$Component) {
  _inherits(Modal, _React$Component);

  var _super = _createSuper(Modal);

  function Modal(_ref) {
    var _this;

    var _ref$title = _ref.title,
        title = _ref$title === void 0 ? 'Thank you' : _ref$title,
        _ref$body = _ref.body,
        body = _ref$body === void 0 ? 'Please give us about 24 hours to get back to you.' : _ref$body,
        _ref$show = _ref.show,
        show = _ref$show === void 0 ? true : _ref$show;

    _classCallCheck(this, Modal);

    _this = _super.call(this);

    _defineProperty(_assertThisInitialized(_this), "handleClose", function () {
      return _this.setState({
        show: false
      });
    });

    _defineProperty(_assertThisInitialized(_this), "handleShow", function () {
      return _this.setState({
        show: true
      });
    });

    _this.state = {
      title: title,
      body: body,
      show: show
    };
    return _this;
  }

  _createClass(Modal, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal, {
        show: this.state.show,
        onHide: this.handleClose,
        backdrop: "static",
        keyboard: false
      }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Dialog, {
        style: {
          width: '100%',
          height: '100%'
        },
        className: "m-0 border-0 p-0"
      }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Header, {
        closeButton: true
      }, /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Title, null, "Modal title")), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Body, null, /*#__PURE__*/_react["default"].createElement("p", null, "Modal body text goes here.")), /*#__PURE__*/_react["default"].createElement(_reactBootstrap.Modal.Footer, null, /*#__PURE__*/_react["default"].createElement(_officeUiFabricReact.Button, {
        variant: "secondary"
      }, "Close"), /*#__PURE__*/_react["default"].createElement(_officeUiFabricReact.Button, {
        variant: "primary"
      }, "Save changes"))));
    } // render() {
    //   return (
    //     <div className="form-popup show">
    //       <div className="form-popup-close-layer"></div>
    //       <div className="form-popup-content">
    //         <div
    //           className="text"
    //           dangerouslySetInnerHTML={{ __html: this.state.body }}
    //         ></div>
    //       </div>
    //     </div>
    //   )
    // }

  }]);

  return Modal;
}(_react["default"].Component);

var ContactForm = /*#__PURE__*/function (_React$Component2) {
  _inherits(ContactForm, _React$Component2);

  var _super2 = _createSuper(ContactForm);

  function ContactForm(props) {
    var _this2;

    _classCallCheck(this, ContactForm);

    _this2 = _super2.call(this, props);

    _defineProperty(_assertThisInitialized(_this2), "pattern", /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);

    _this2.state = {
      name: 'testing',
      email: 'your@email.com',
      subject: 'Test',
      message: 'Testing 1234',
      isInvalid: false
    };
    return _this2;
  }

  _createClass(ContactForm, [{
    key: "validate",
    value: function validate() {
      var _ref2 = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
        current: {
          set: false
        }
      },
          current = _ref2.current;

      var c = this.state;

      if (current.set) {
        c = _objectSpread(_objectSpread({}, c), current);
        delete c.set;
      }

      if ("".concat(c.name).trim().length < 3) {
        this.setState({
          isInvalid: true
        });
        return;
      }

      if (!this.pattern.test("".concat(c.email))) {
        this.setState({
          isInvalid: true
        });
        return;
      }

      if ("".concat(c.subject).trim().length < 5) {
        this.setState({
          isInvalid: true
        });
        return;
      }

      if ("".concat(c.message).trim().length < 10) {
        this.setState({
          isInvalid: true
        });
        return;
      }

      this.setState({
        isInvalid: false
      });
    }
  }, {
    key: "submit",
    value: function submit() {
      window.jQuery('.form-popup .text').text('Thank You for contacting us!');
      $('.form-popup').fadeIn(300);
      formPopupTimeout = setTimeout(function () {
        $('.form-popup').fadeOut(300);
      }, 3000);
      $('.contact-form').append('<input type="reset" class="reset-button"/>');
      $('.reset-button').click().remove();
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      return /*#__PURE__*/_react["default"].createElement("div", {
        className: "container"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "row page-tagline"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-md-6 col-md-offset-3 wow flipInX",
        "data-wow-delay": "0.3s"
      }, /*#__PURE__*/_react["default"].createElement("h2", {
        className: "title"
      }, "get in touch"), /*#__PURE__*/_react["default"].createElement("div", {
        className: "description"
      }, ' ', "Give us a call today and you'll be glad you did."))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "row"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-md-3 wow fadeInLeft",
        "data-wow-delay": "0.3s"
      }, /*#__PURE__*/_react["default"].createElement("div", {
        className: "contact-thumbs"
      }, /*#__PURE__*/_react["default"].createElement("img", {
        src: "img/theme-1/icon7.png",
        alt: ""
      }), /*#__PURE__*/_react["default"].createElement("article", {
        className: "normall"
      }, /*#__PURE__*/_react["default"].createElement("p", null, "Tel:", /*#__PURE__*/_react["default"].createElement("br", null), /*#__PURE__*/_react["default"].createElement("a", {
        href: "tel:43477596204"
      }, "+234 808.847.3115")))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "contact-thumbs"
      }, /*#__PURE__*/_react["default"].createElement("img", {
        src: "img/theme-1/icon8.png",
        alt: ""
      }), /*#__PURE__*/_react["default"].createElement("article", {
        className: "normall"
      }, /*#__PURE__*/_react["default"].createElement("p", null, "Email:", /*#__PURE__*/_react["default"].createElement("br", null), /*#__PURE__*/_react["default"].createElement("a", {
        href: "maito:info@cradlesoft.tech"
      }, "info@cradlesoft.tech"))))), /*#__PURE__*/_react["default"].createElement("div", {
        className: "col-md-7 col-md-offset-1 wow fadeInRight",
        "data-wow-delay": "0.3s"
      }, /*#__PURE__*/_react["default"].createElement("form", {
        className: "contact-form",
        method: "post"
      }, /*#__PURE__*/_react["default"].createElement("input", {
        className: "required",
        type: "text",
        placeholder: "Your name",
        value: this.state.name,
        onChange: function onChange(e) {
          _this3.setState({
            name: e.currentTarget.value
          });

          _this3.validate({
            current: _objectSpread(_objectSpread({}, _this3.state), {}, {
              name: e.currentTarget.value,
              set: true
            })
          });
        },
        name: "name"
      }), /*#__PURE__*/_react["default"].createElement("input", {
        className: "required",
        type: "email",
        placeholder: "Your email",
        value: this.state.email,
        onChange: function onChange(e) {
          _this3.setState({
            email: e.currentTarget.value
          });

          _this3.validate({
            current: _objectSpread(_objectSpread({}, _this3.state), {}, {
              email: e.currentTarget.value,
              set: true
            })
          });
        },
        name: "email"
      }), /*#__PURE__*/_react["default"].createElement("input", {
        type: "text",
        placeholder: "Subject",
        value: this.state.subject,
        name: "subject",
        onChange: function onChange(e) {
          _this3.setState({
            subject: e.currentTarget.value,
            set: true
          });

          _this3.validate({
            current: _objectSpread(_objectSpread({}, _this3.state), {}, {
              subject: e.currentTarget.value
            })
          });
        },
        style: {
          width: 'calc(94% - 0px)'
        }
      }), /*#__PURE__*/_react["default"].createElement("textarea", {
        className: "required",
        placeholder: "Your message",
        name: "text",
        value: this.state.message,
        onChange: function onChange(e) {
          _this3.setState({
            message: e.currentTarget.value
          });

          _this3.validate({
            current: _objectSpread(_objectSpread({}, _this3.state), {}, {
              message: e.currentTarget.value,
              set: true
            })
          });
        }
      }), /*#__PURE__*/_react["default"].createElement("div", {
        className: "submit-wraper"
      }, /*#__PURE__*/_react["default"].createElement("button", {
        className: "button",
        style: {
          border: 'none'
        },
        disabled: this.state.isInvalid,
        onClick: function onClick() {
          return _this3.submit;
        }
      }, "submit message")), /*#__PURE__*/_react["default"].createElement("input", {
        type: "hidden",
        name: "mailto",
        value: "info@cradlesoft.tech",
        style: {
          display: 'none'
        }
      }))), /*#__PURE__*/_react["default"].createElement(Modal, null)));
    }
  }]);

  return ContactForm;
}(_react["default"].Component);

var domContainer = document.querySelector('#contact');

_reactDom["default"].render( /*#__PURE__*/_react["default"].createElement(ContactForm, null), document.getElementById('contact'));
//# sourceMappingURL=App.js.map
