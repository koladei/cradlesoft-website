import React from 'react'
import { Modal as BootstrapModal, Button } from 'react-bootstrap'

import './App.css'

export default class CustomModal extends React.Component {
  constructor({
    title = 'Thank you',
    body = 'Please give us about 24 hours to get back to you.',
    show = false,
    onClose = () => {},
  }) {
    super()
    this.state = {
      show,
    }
  }
  handleClose = () => {
    this.setState({ show: false })

    if (typeof this.props.onClose == 'function') {
      this.props.onClose()
    }
  }

  componentDidUpdate = (prevProps) => {
    if (this.state.show != this.props.show)
      this.setState({ show: this.props.show })
  }

  render() {
    return (
      <BootstrapModal
        show={this.props.show}
        onHide={this.handleClose}
        backdrop="static"
        keyboard={false}
      >
        <BootstrapModal.Dialog
          style={{ width: '100%', height: '100%' }}
          className="m-0 border-0 p-0 rounded-0"
        >
          <BootstrapModal.Header closeButton>
            <BootstrapModal.Title>{this.props.title}</BootstrapModal.Title>
          </BootstrapModal.Header>

          <BootstrapModal.Body style={{ border: 'none' }}>
            <div dangerouslySetInnerHTML={{ __html: this.props.body }} />
          </BootstrapModal.Body>

          <BootstrapModal.Footer>
            <button
              className="button border-0 rounded-0"
              variant="primary"
              onClick={this.handleClose}
            >
              Ok
            </button>
          </BootstrapModal.Footer>
        </BootstrapModal.Dialog>
      </BootstrapModal>
    )
  }
}
