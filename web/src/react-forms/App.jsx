import React from 'react'
import ReactDOM from 'react-dom'
import { Modal as BootstrapModal, Button } from 'react-bootstrap'
import CustomModal from './CustomModal.js'

class ContactForm extends React.Component {
  pattern = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i

  constructor(props) {
    super(props)
    this.state = {
      name: 'testing',
      email: 'your@email.com',
      subject: 'Test',
      message: 'Testing 1234',
      phone: 'Testing 1234',
      isInvalid: false,
      showModal: false,
      response: {
        loading: false,
        title: '',
        body: '',
      },
    }
  }

  validate({ current } = { current: { set: false } }) {
    let c = this.state

    if (current.set) {
      c = { ...c, ...current }
      delete c.set
    }

    if (`${c.name}`.trim().length < 3) {
      this.setState({ isInvalid: true })
      return
    }

    if (!this.pattern.test(`${c.email}`)) {
      this.setState({ isInvalid: true })
      return
    }

    if (`${c.phone}`.trim().length < 10) {
      this.setState({ isInvalid: true })
      return
    }

    if (`${c.subject}`.trim().length < 5) {
      this.setState({ isInvalid: true })
      return
    }

    if (`${c.message}`.trim().length < 10) {
      this.setState({ isInvalid: true })
      return
    }

    this.setState({ isInvalid: false })
  }

  submit = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        response: { ...prevState.response, loading: true },
      }
    })

    return fetch(
      'https://cradlesoftwebsite.azurewebsites.net/api/enquirysubmitted?code=2J4Ky3XdA9cPrkaQgdoKjZ5K5KR2XXyLuE2DpMHpixpRriuMvZLD2g%3D%3D',
      {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          name: this.state.name,
          email: this.state.email,
          phone: this.state.phone,
          subject: this.state.subject,
          message: this.state.message,
        }),
      },
    )
      .then((res) => res.json())
      .then((res) => {
        if (res.status == 'success') {
          this.setState((prevState) => {
            let newState = {
              ...prevState,
              showModal: true,
              name: '',
              phone: '',
              email: '',
              subject: '',
              message: '',
              isInvalid: false,
              response: {
                ...prevState.response,
                loading: false,
                title: 'Thank you',
                body: res.message,
              },
            }

            return newState
          })

          formPopupTimeout = setTimeout(function () {
            this.setState((prevState) => {
              let newState = {
                ...prevState,
                response: {
                  ...prevState.response,
                  loading: false,
                  title: '',
                  body: '',
                },
              }

              return newState
            })
          }, 3000)
        } else {
          // alert(`ERROR: ${res.message}`)
          this.setState((prevState) => {
            let newState = {
              ...prevState,
              isInvalid: true,
              showModal: true,
              response: {
                ...prevState.response,
                loading: false,
                title: 'Opps!',
                body: `An error occurred: <b>${res.message}</b>`,
              },
            }

            return newState
          })
        }
      })
  }

  render() {
    return (
      <div className="container">
        <div className="row page-tagline">
          <div
            className="col-md-6 col-md-offset-3 wow flipInX"
            data-wow-delay="0.3s"
          >
            <h2 className="title">get in touch</h2>
            <div className="description">
              {' '}
              Give us a call today and you'll be glad you did.
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-3 wow fadeInLeft" data-wow-delay="0.3s">
            <div className="contact-thumbs">
              <img src="img/theme-1/icon7.png" alt="" />
              <article className="normall">
                <p>
                  Tel:
                  <br />
                  <a href="tel:43477596204">+234 808.847.3115</a>
                </p>
              </article>
            </div>
            <div className="contact-thumbs">
              <img src="img/theme-1/icon8.png" alt="" />
              <article className="normall">
                <p>
                  Email:
                  <br />
                  <a href="maito:info@cradlesoft.tech">info@cradlesoft.tech</a>
                </p>
              </article>
            </div>
          </div>
          <div
            className="col-md-7 col-md-offset-1 wow fadeInRight"
            data-wow-delay="0.3s"
          >
            <form className="contact-form" method="post">
              <input
                className="required"
                type="text"
                placeholder="Your name"
                value={this.state.name}
                onChange={(e) => {
                  this.setState({ name: e.currentTarget.value })
                  this.validate({
                    current: {
                      ...this.state,
                      name: e.currentTarget.value,
                      set: true,
                    },
                  })
                }}
                name="name"
              />
              <input
                className="required"
                type="email"
                placeholder="Your email"
                value={this.state.email}
                onChange={(e) => {
                  this.setState({ email: e.currentTarget.value })
                  this.validate({
                    current: {
                      ...this.state,
                      email: e.currentTarget.value,
                      set: true,
                    },
                  })
                }}
                name="email"
              />
              <input
                type="text"
                placeholder="Subject"
                value={this.state.subject}
                name="subject"
                onChange={(e) => {
                  this.setState({ subject: e.currentTarget.value, set: true })
                  this.validate({
                    current: {
                      ...this.state,
                      subject: e.currentTarget.value,
                      set: true,
                    },
                  })
                }}
              />
              <input
                type="tel"
                placeholder="Phone"
                value={this.state.phone}
                name="phone"
                onChange={(e) => {
                  this.setState({ phone: e.currentTarget.value, set: true })
                  this.validate({
                    current: {
                      ...this.state,
                      phone: e.currentTarget.value,
                      set: true,
                    },
                  })
                }}
              />
              <textarea
                className="required"
                placeholder="Your message"
                name="text"
                value={this.state.message}
                onChange={(e) => {
                  this.setState({
                    message: e.currentTarget.value,
                  })
                  this.validate({
                    current: {
                      ...this.state,
                      message: e.currentTarget.value,
                      set: true,
                    },
                  })
                }}
              ></textarea>
              <div className="submit-wraper">
                <button
                  className="align-items-center button d-flex justify-content-between mx-2"
                  style={{ border: 'none' }}
                  disabled={
                    this.state.isInvalid || this.state.loading
                  }
                  onClick={(e) => {
                    e.preventDefault()
                    this.submit()
                  }}
                >
                  {this.state.response.loading ? (
                    <div
                      className="spinner-border  spinner-border-sm text-light mr-3"
                      role="status"
                    ></div>
                  ) : (
                    ''
                  )}
                  submit message
                </button>
              </div>
              <input
                type="hidden"
                name="mailto"
                value="info@cradlesoft.tech"
                style={{ display: 'none' }}
              />
            </form>
          </div>
          <CustomModal
            show={this.state.showModal}
            title={this.state.response.title}
            body={this.state.response.body}
            onClose={() => {
              this.setState({ showModal: false, isInvalid: true })
            }}
          />
        </div>
      </div>
    )
  }
}

const domContainer = document.querySelector('#contact')
ReactDOM.render(<ContactForm />, document.getElementById('contact'))
