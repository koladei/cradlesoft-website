// Connect to the database
const { MongoClient } = require('mongodb')
const { v4: uuidv4 } = require('uuid')
const url = process.env['COSMOS_MONGODB_CONNECTION_STRING']
const pattern = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i

const getCollections = async () => {
  const client = new MongoClient(url)

  await client.connect()
  const database = client.db('CRADLESOFT')
  const contactCollection = database.collection('contact')

  return { contactCollection }
}

const validateInput = ({
  name = '',
  email = '',
  subject = '',
  message = '',
  phone = '',
}) => {
  if (`${name}`.trim().length < 3) {
    return {
      isValid: false,
      message: 'Please provide your name',
    }
  }

  if (!pattern.test(`${email}`)) {
    return {
      isValid: false,
      message: 'Please provide a valid email address',
    }
  }

  if (`${phone}`.trim().length < 9) {
    return {
      isValid: false,
      message: 'Please provide a valid phone number',
    }
  }

  if (`${subject}`.trim().length < 5) {
    return {
      isValid: false,
      message: 'Your request/enquiry title is too short.',
    }
  }

  if (`${message}`.trim().length < 10) {
    return {
      isValid: false,
      message: 'Your message is too short',
    }
  }

  return {
    isValid: true,
  }
}

module.exports = async function (context, req) {
  context.res = {
    // status: 200, /* Defaults to 200 */u
    headers: {
      'Content-Type': 'application/json',
    },
    body: { status: 'failure' },
  }

  // validate the input
  const validation = validateInput({ ...req.body })
  context.log('Validation: ', JSON.stringify(validation))
  if (validation.isValid) {
    context.log('About to retrieve collection')
    const { contactCollection } = await getCollections()
    context.log('Retrieved retrieve collection')

    const result = await contactCollection.insertOne({
      category: 'enquiry',
      name: req.body.name,
      email: req.body.email,
      subject: req.body.subject,
      message: req.body.message,
      phone: req.body.phone,
    })

    context.log('Checking result', JSON.stringify(result))
    if (result.ops && result.ops.length > 0) {
      context.res = {
        ...context.res,
        body: {
          status: 'success',
          message: 'Your request/enquiry has been received',
        },
      }
    }
  }

  if (context.res.body.status != 'success')
    context.res.body = { ...context.res.body, message: validation.message }
}
